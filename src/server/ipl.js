const fs = require('fs');


// CSV File paths
const DELIVERIES_CSV_PATH = './src/data/deliveries.csv';
const MATCHES_CSV_PATH = './src/data/matches.csv';


// This is a utility function used to return 2D list of rows in file.
function giveLinesFromFile(path) {

    const fileBuffer = fs.readFileSync(path);
    const fileData = fileBuffer.toString();
    let lines = fileData.split('\n');

    // Make lines a 2D list
    lines = lines.map(line => line.split(','));

    return lines;
}


// Number OF matches played per year for all Seasons
function numberOfMatchesPlayed() {

    // Read the file 
    let lines = giveLinesFromFile(MATCHES_CSV_PATH);

    let matchesPlayed = {};

    for (let row of lines.slice(1)) {
        const season = row[1];   // get season

        // IF season already in the object
        if (season in matchesPlayed){
            matchesPlayed[season] += 1;
        } else {
            matchesPlayed[season] = 1;
        }
    }

    return matchesPlayed;
};


// Function to return number of matches won per team per IPL
function numberOfMatchesWon() {

    // Read the file
    let lines = giveLinesFromFile(MATCHES_CSV_PATH);

    let matchesWon = {};

    for (row of lines.slice(1)){
        let season = row[1];
        let winner = row[10];

        // IF season is alread in object
        if (season in matchesWon) {

            // If winner team has a record
            if (winner in matchesWon[season]) {
                matchesWon[season][winner] += 1;
            } else {
                matchesWon[season][winner] = 1;
            }

        } else {
            matchesWon[season] = {};
            matchesWon[season][winner] = 1;
        }
    };

    return matchesWon;
}


// Function to get extra runs per team in a year
function extraRunForTeam(year = 2016) {

    // Read the file matches csv to get matchIDs for a year
    let lines = giveLinesFromFile(MATCHES_CSV_PATH);

    let matchIds = new Set()

    for (let row of lines.slice(1)) {

        let matchId = row[0];
        let matchYear = row[1];

        if (matchYear == year) {
            matchIds.add(parseInt(matchId));
        }
    }

    // Now read the deliveries file
    lines = giveLinesFromFile(DELIVERIES_CSV_PATH);

    let extraRunsOfTeam = {}

    for (let row of lines.slice(1)){

        let matchId = parseInt(row[0]);
        let extraRuns = parseInt(row[16]);
        let team = row[3];

        // Check for matches in matchIds only
        if (matchIds.has(matchId)) {

            // If team is already in resutls
            if (team in extraRunsOfTeam) {
                extraRunsOfTeam[team] += extraRuns;
            } else {
                extraRunsOfTeam[team] = extraRuns;
            }
        }
    }

    return extraRunsOfTeam;
}


// Top 10 Economical Bowler of the year
function economicalBowlerOfYear(year = 2015, topN = 10) {

    // Read the file matches csv to get matchIDs for a year
    let lines = giveLinesFromFile(MATCHES_CSV_PATH);

    let matchIds = new Set()

    for (let row of lines.slice(1)) {

        let matchId = row[0];
        let matchYear = row[1];

        if (matchYear == year) {
            matchIds.add(parseInt(matchId));
        }
    }

    lines = giveLinesFromFile(DELIVERIES_CSV_PATH);

    let bowlers = {};

    for (let row of lines.slice(1)){

        let matchId = parseInt(row[0]);
        let totalRuns = parseInt(row[17]);
        let bowler = row[8];

        // Check for matches in matchIds only
        if (matchIds.has(matchId)) {

            // If bowler is already in resutls
            if (bowlers[bowler]) {
                bowlers[bowler]['runs'] += totalRuns;
                bowlers[bowler]['balls'] += 1;
            } else {
                bowlers[bowler] = {'runs': totalRuns, 'balls': 1};
            }
        }
    }

    // Get the economy of all bowlers
    bowlers = Object.entries(bowlers).map( item => {
        item[1]['economy'] = item[1].runs * 6.0 / item[1].balls;
        return item
    })

    // Sort by the economy of bowlers
    bowlers = bowlers.sort((a, b) => a[1].economy - b[1].economy);

    // Filter bowlers which have atleast bowled 50 balls
    bowlers = bowlers.filter(item => item[1].balls > 50);

    // Return top N bowlers
    return bowlers.slice(0, topN);
}



// Dump Number of matches played into json
fs.writeFile('./src/public/output/numberOfMatchesPlayed.json',
JSON.stringify(numberOfMatchesPlayed()), (err) => {
    if(err) {
        console.log(err);
    }
});

// Dump number Of Matches Won
fs.writeFile('./src/public/output/numberOfMatchesWon.json',
JSON.stringify(numberOfMatchesWon()), (err) => {
    if(err) {
        console.log(err);
    }
})

// Dump Extra Runs per team
fs.writeFile('./src/public/output/extraRunForTeam.json',
JSON.stringify(extraRunForTeam()), (err) => {
    if(err) {
        console.log(err);
    }
})

// Dump the economical Bowler Of Year
fs.writeFile('./src/public/output/economicalBowlerOfYear.json',
JSON.stringify(economicalBowlerOfYear()), (err) => {
    if(err){
        console.log(err);
    }
})